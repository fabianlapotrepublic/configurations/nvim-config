#!/bin/bash
source ./bash-utils/utils.bash

BINARY_INSTALLATION="neovim"

echo "Installing dependancy need for fab's ${BINARY_INSTALLATION} config"

#TODO: find a solution because package golang is golang-go and silversearcher-ag in apt repository
install_packages "neovim" "python3-neovim" "fz" "ripgrep" "the_silver_searcher" "nodejs" "golang" "ninja-build" "bat" "git-delta" "clang-12"

# install utility
# install plug to for installing extention
PLUG_PATH="${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/autoload/plug.vim"
if [ ! -f ${PLUG_PATH} ]; then
  curl -fLo ${PLUG_PATH} --create-dirs "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
fi

# as we install node with package manager we need to change localisation to avoid giving root privilages when installing package
npm config set prefix '~/.npm-global'

# install language server use by my neovim configuration
npm i -g bash-language-server pyright vim-language-server typescript typescript-language-server yaml-language-server ansible-language-server
if [ ! $? ];then
  echo "fail to install lsp server with npm"
  exit 1
fi

go install golang.org/x/tools/gopls@latest
if [ ! $? ];then
  echo "fail to install gopls"
  exit 1
fi

###################### User installation #####################################

if [ ! -d "/etc/compleat.d" ];then
  # install compleat
  COMPLEAT_PATH=${HOME}/Tools/compleat
  EXECUTABLE="stack"
  EXEC_PATH=$(which ${EXECUTABLE})
  EXEC_PATH_EXIT_STATE=$?
  if [ ! ${EXEC_PATH_EXIT_STATE} = 0 ];then
    curl -sSL https://get.haskellstack.org/ | sh
  fi
  DIR_PATH=$(pwd)
  git clone git://github.com/mbrubeck/compleat.git ${COMPLEAT_PATH} && cd ${COMPLEAT_PATH} && make install
  cd DIR_PATH
  sudo mkdir /etc/compleat.d
  sudo cp examples/* /etc/compleat.d
fi

if [ ! -d "${HOME}/Tools/lua-language-server" ];then
  git clone https://github.com/sumneko/lua-language-server ${HOME}/Tools/lua-language-server
  DIR_PATH=$(pwd)
  cd ${HOME}/Tools/lua-language-server
  git submodule update --init --recursive
  cd 3rd/luamake
  ./compile/install.sh
  cd ../..
  ./3rd/luamake/luamake rebuild
  cd ${DIR_PATH}
fi

######################## System installation ###################################

# install ansible-lint use by ansible client
EXECUTABLE="ansible-lint"
EXEC_PATH=$(which ${EXECUTABLE})
EXEC_PATH_EXIT_STATE=$?
if [ ! ${EXEC_PATH_EXIT_STATE} = 0 ];then
  pip3 install "ansible-lint[community,yamllint]"
fi

NVIM_PATH="${HOME}/.config/nvim"
if [ -h ${NVIM_PATH} ]; then
  rm -rf ${NVIM_PATH}
elif [ -e ${NVIM_PATH} ]; then
  mv ${NVIM_PATH} "${NVIM_PATH}-old"
fi
ln -s "$(pwd)/nvim/." ${NVIM_PATH}

echo "You can start using ${BINARY_INSTALLATION} with lsp support for golang, python, typescript ..."
