# nvim

This repo as fab's neovim configuration. 

## Prerequested

1. nodejs
2. golang
3. vim-plug
4. Java
5. Packer[https://github.com/wbthomason/packer.nvim]

After prerequest are install just run `./install.sh`

```vim
:PackerInstall
```

### Language Server Protocl server

1. clangd-12
2. bash-language-server
3. pyright
4. vim-language-server
5. typescript-language-server
6. gopls

