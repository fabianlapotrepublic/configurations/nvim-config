
local config = {
  -- The command that starts the language serverjj
  on_attach = on_attach,
  capabilities = capabilities,
  cmd = {
    '/home/fabianlapotre/.asdf/shims/java',
    '-Declipse.application=org.eclipse.jdt.ls.core.id1',
    '-Dosgi.bundles.defaultStartLevel=4',
    '-Declipse.product=org.eclipse.jdt.ls.core.product',
    '-Dlog.level=ALL',
    '-noverify',
    '-Xmx1G',
    '-jar', '/usr/share/jdtls/plugins/org.eclipse.equinox.launcher_1.6.400.v20210924-0641.jar',
    '-configuration', '/usr/share/jdtls/config_linux',
    '-data', '/home/fabian/Test/',
    '--add-modules=ALL-SYSTEM',
    '--add-opens java.base/java.util=ALL-UNNAMED',
    -- ADD REMAINING OPTIONS FROM https://github.com/eclipse/eclipse.jdt.ls#running-from-the-command-line !
  },
  filetypes = { "java"},
  -- This is the default if not provided, you can remove it. Or adjust as needed.
  -- One dedicated LSP server & client will be started per unique root_dir
  root_dir = require('jdtls.setup').find_root({'.git','mvnw', 'gradlew'}),
  settings = {
      java = {
      }
  }
}
require('jdtls').start_or_attach(config)
