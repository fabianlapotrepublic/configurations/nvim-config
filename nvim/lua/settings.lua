-- define theme I'm using
vim.cmd [[colorscheme dracula]]

-- start tree plugin to move around project
require 'nvim-tree'.setup()

local scrollbarColor = require 'dracula'.colors().purple
-- all to have scroll indicator
require 'scrollbar'.setup({
	handle = {
		color = scrollbarColor
	}
})

-- define list char to see how file is structure
vim.opt.list = true
vim.opt.listchars = {
	space = "•",
	eol = "↴",
	tab = "▸▸",
	trail = "࿉",
	extends = "❯",
	precedes = "❮",
	nbsp = "_",
}

-- make tab width of 2 space
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.bo.softtabstop = 2

-- add number add the begining of the line
vim.opt.number = true
vim.opt.relativenumber = true

-- update plugin with packer when a plugin is add
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])
