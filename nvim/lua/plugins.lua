vim.cmd [[packadd packer.nvim]]

-- make packer install package automaticly
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

require("packer").startup(function(use)

  use 'wbthomason/packer.nvim'
  use 'ray-x/go.nvim'
  -- use by GoLint
  use {'ray-x/guihua.lua', run = 'cd lua/fzy && make'}

  -- linter
  use 'dense-analysis/ale'


  use 'nvim-treesitter/nvim-treesitter'
  use {
    'nvim-telescope/telescope.nvim',
    requires = { { 'nvim-lua/plenary.nvim' } }
  }
  use {
    'kyazdani42/nvim-tree.lua',
    requires = {
      'kyazdani42/nvim-web-devicons', -- optional, for file icons
    },
    config = function() require 'nvim-tree'.setup {} end
  }
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }
  use 'akinsho/toggleterm.nvim'
  use 'Mofiqul/dracula.nvim'
  use {
    'phaazon/hop.nvim',
    branch = 'v1', -- optional but strongly recommendedVariantOrder
    config = function()
      -- you can configure Hop the way you like here; see :h hop-configrequire
      require 'hop'.setup { keys = 'etovxqpdygfblzhckisuran' }
    end
  }

  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/vim-vsnip'
  use 'hrsh7th/vim-vsnip-integ'
  use 'petertriho/nvim-scrollbar'
  if packer_bootstrap then
    require('packer').sync()
  end
end)
