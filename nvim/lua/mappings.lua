
-- Personal mapping
vim.g.mapleader = ' '
vim.api.nvim_set_keymap('n', '<Leader><CR>', ':e ~/.config/nvim<CR>', { noremap = true })
vim.api.nvim_set_keymap('i', 'jk', '<Esc>', { noremap = true })

-- NvimTree mapping
vim.api.nvim_set_keymap('n', '<Leader>pv', ':NvimTreeToggle<CR>', { noremap = true })


-- hop (motion) configuration
vim.api.nvim_set_keymap('n', 'f',
	"<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true })<cr>"
	, {})
vim.api.nvim_set_keymap('n', 'F',
	"<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true })<cr>"
	, {})
vim.api.nvim_set_keymap('o', 'f',
	"<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true, inclusive_jump = true })<cr>"
	, {})
vim.api.nvim_set_keymap('o', 'F',
	"<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true, inclusive_jump = true })<cr>"
	, {})
vim.api.nvim_set_keymap('', 't',
	"<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.AFTER_CURSOR, current_line_only = true })<cr>"
	, {})
vim.api.nvim_set_keymap('', 'T',
	"<cmd>lua require'hop'.hint_char1({ direction = require'hop.hint'.HintDirection.BEFORE_CURSOR, current_line_only = true })<cr>"
	, {})
vim.api.nvim_set_keymap('', '<Leader>hw', "<cmd> lua require'hop'.hint_words()<cr>", {})
vim.api.nvim_set_keymap('', '<Leader>hl', "<cmd> lua require'hop'.hint_lines()<cr>", {})
vim.api.nvim_set_keymap('', '<Leader>hp', "<cmd> lua require'hop'.hint_patterns()<cr>", {})
